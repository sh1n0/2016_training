(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();

    // Modal表示
    $('.modal-trigger').leanModal();

    // カレンダー表示
    $('.datepicker').pickadate({
      selectMonths: true,
      selectYears: 15
    });

    // submit押下時、card追加
    $('#submit-btn').on('click', function(){
      if ($('#todo').val() == '' || $('#detail').val() == '' || $('#place').val() == '' || $('#date').val() == '') {
        Materialize.toast('Please enter', 3000, 'rounded');
        return false;
      } else {
        var html_row = '<div class="row">';
        var html_col = '<div class="col s12 m6" id="before_card">';
        var html_card = '<div class="card">';
        var html_card_content = '<div class="card-content z-depth-3">';
        var html_div_end = '</div>';
        var html_title = '<span class="card-title">' + $('#todo').val() + '</span>';
        var html_table = '<table>';
        var html_table_end = '</table>';
        var html_detail = '<tr><td>' + $('#detail').val() + '</td></tr>';
        var html_place = '<tr><td>' + $('#place').val() + '</td></tr>';
        var html_date = '<tr><td>' + $('#date').val() + '</td></tr>';
        var html_lever = '<tr><td><div class="switch"><label>' + 'Off' + '<input type="checkbox"><span class="lever"></span>' + 'On' + '</lavel></div></td></tr>';
        if ($('.card').length) {
          $('#before_card').before(html_col + html_card + html_card_content + html_title + html_table + html_detail + html_place + html_date + html_lever + html_table_end + html_div_end + html_div_end + html_div_end);
        } else {
          $('#add_card').after(html_row + html_col + html_card + html_card_content + html_title + html_table + html_detail + html_place + html_date + html_lever + html_table_end + html_div_end + html_div_end + html_div_end + html_div_end);
        }
        $('#todo').val('');
        $('#detail').val('');
        $('#place').val('');
        $('#date').val('');
      }
    });

  });
})(jQuery);
